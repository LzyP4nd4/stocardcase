module.exports = {
  toRadians(degrees) {
    return degrees * (Math.PI / 180);
  },
  // https://www.movable-type.co.uk/scripts/latlong.html Spherical Law of Cosines, calculates distance in meters
  calculateDistance(coords1, coords2) {
    const φ1 = this.toRadians(coords1.lat);
    const φ2 = this.toRadians(coords2.lat);
    const Δλ = this.toRadians((coords2.lon - coords1.lon));
    const R = 6371e3; // in meters
    const d = Math.acos(Math.sin(φ1) * Math.sin(φ2) + Math.cos(φ1) * Math.cos(φ2) * Math.cos(Δλ)) * R;
    return d;
  },
};
