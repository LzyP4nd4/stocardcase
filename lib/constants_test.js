module.exports = Object.freeze({
  /**
   * coordinates used for getting Mannheim & Altrip, for test purposes only
   */
  TEST_COORD: {
    lat: 49.48,
    lon: 8.46,
  },
  /**
   * city id of Mannheim, for test purposes only
   */
  TEST_CITYID: 2873891,
});
