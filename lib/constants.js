module.exports = Object.freeze({
  BADREQUEST_ERROR_MESSAGE: 'lat/lng required',
  BADREQUEST_ERROR: 'BadRequest',
  NOTFOUND_ERROR_MESSAGE: 'not found',
  NOTFOUND_ERROR: 'NotFound',
  /**
   * maximum distance (10000m) between two coordinates for filtering nearby cities
   */
  MAXRADIUS: 10000,
});
