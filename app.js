/* eslint-env node */
require('dotenv').config({ path: 'config/.env' }); // Load API Keys from .env file
const restify = require('restify');
const citiesAPI = require('./cities/citiesAPI');

const server = restify.createServer();
server.use(restify.plugins.queryParser());
server.get('/cities', citiesAPI.routeToCitiesInCycle);
server.get('/cities/:cityId', citiesAPI.routeToCityDetails);
server.get('/cities/:cityId/weather', citiesAPI.routeToWeather);


server.listen(8080, () => {
  console.log('%s listening at %s', server.name, server.url);
});
