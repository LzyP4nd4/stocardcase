/* eslint-env node */
const request = require('request');
const citiesData = require('../lib/city.list.json');
const utils = require('../lib/utils');
const constants = require('../lib/constants');

module.exports = {
  /**
   * gets all cities around 10km of the coordinates
   * @param {json} coord contains latitude (lat) and longitude (lon)
   * @param {function} callbackFunc callback for the return values
   * @returns array of json objects with cities within the radius
   */
  findCitiesInCycle(
    coord,
    callbackFunc,
  ) {
    const citiesArray = [];
    citiesData.forEach((city) => {
      if (utils.calculateDistance(coord, city.coord) <= constants.MAXRADIUS) {
        citiesArray.push({
          id: city.id,
          name: city.name,
        });
      }
    });
    return callbackFunc(citiesArray);
  },
  /**
   * Gets details of a city by city id
   * @param {number} cityId id of the target city
   * @param {function} callbackFunc callback for the return value
   * @returns json object with city details or error message if not found
   */
  getCityDetails(
    cityId,
    callbackFunc,
  ) {
    const city = citiesData.find(element => element.id === cityId);
    if (city !== undefined) { // city found
      return callbackFunc(city);
    }
    return callbackFunc(undefined);
  },
  /**
   * Gets the weather of a city by city id
   * @param {number} cityId id of the target city
   * @param {*} callbackFunc callback for return value
   * @returns json object with the weather of the city
   */
  getWeather(
    cityId,
    callbackFunc,
  ) {
    request({
      url: `http://api.openweathermap.org/data/2.5/weather?id=${cityId}&appid=${process.env.OpenWeatherMapsApiKey}&units=metric`,
      json: true,
    }, (error, response, body) => {
      if (body.cod === 200) { // city found
        const weather = {
          type: body.weather[0].main,
          type_description: body.weather[0].description,
          sunrise: (new Date(body.sys.sunrise * 1000)).toISOString(),
          sunset: (new Date(body.sys.sunset * 1000)).toISOString(),
          temp: body.main.temp,
          temp_min: body.main.temp_min,
          temp_max: body.main.temp_max,
          pressure: body.main.pressure,
          humidity: body.main.humidity,
          clouds_percent: body.clouds.all,
          wind_speed: body.wind.speed,
        };
        return callbackFunc(weather);
      }
      return callbackFunc(undefined);
    });
  },
};
