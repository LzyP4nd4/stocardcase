/* eslint-env node, mocha */
require('dotenv').config({ path: 'config/.env' }); // Load API Keys from .env file
const assert = require('assert');
const sinon = require('sinon');
const cities = require('../cities');
const citiesAPI = require('../citiesAPI');
const constants = require('../../lib/constants');
const testConstants = require('../../lib/constants_test');

describe('cities', () => {
  describe('findCitiesInCycle(coord)', () => {
    it('should return an array containing Mannheim and Altrip, if called with lat=49.48 and lng=8.46', (done) => {
      cities.findCitiesInCycle(
        testConstants.TEST_COORD,
        (citiesInCycle) => {
          assert.strictEqual(citiesInCycle.some(item => item.name === 'Mannheim'), true);
          assert.strictEqual(citiesInCycle.some(item => item.name === 'Altrip'), true);
          assert.strictEqual(citiesInCycle.some(item => item.name === 'Berlin'), false);
          done();
        },
      );
    });
  });
  describe('getCityDetails(cityId)', () => {
    it('should return mannheim when used with TEST_CITYID ', (done) => {
      cities.getCityDetails(
        testConstants.TEST_CITYID,
        (city) => {
          assert.strictEqual(city.name, 'Mannheim');
          done();
        },
      );
    });
  });
  describe('getWeather(cityId)', () => {
    it('should return weather data of mannheim when used with TEST_CITYID', (done) => {
      cities.getWeather(
        testConstants.TEST_CITYID,
        (weather) => {
          // this should be more likely tested with a mock return using sinon
          assert.strictEqual(weather.type !== undefined, true);
          assert.strictEqual(weather.temp !== undefined, true);
          done();
        },
      );
    });
  });
});

describe('citiesAPI', () => {
  describe('routeToCitiesInCycle(req,res,next)', () => {
    it('should return BadRequest and error message if parameters are missing ', (done) => {
      const req = {
        query: {
          lat: testConstants.TEST_COORD.lat,
        },
      };
      citiesAPI.routeToCitiesInCycle(req, null, (error) => {
        assert.equal(error.message, constants.BADREQUEST_ERROR_MESSAGE);
        assert.equal(error.code, constants.BADREQUEST_ERROR);
        done();
      });
    });
    it('should call findCitiesInCycle once', (done) => {
      const req = {
        query: {
          lat: testConstants.TEST_COORD.lat,
          lng: testConstants.TEST_COORD.lon,
        },
      };
      sinon.stub(cities, 'findCitiesInCycle');
      citiesAPI.routeToCitiesInCycle(req, null, () => {}); // do nothing in next
      sinon.assert.calledOnce(cities.findCitiesInCycle); // check if it was exactly called once
      cities.findCitiesInCycle.restore(); // restore original functionality
      done();
    });
  });
  describe('routeToCityDetails(req,res,next)', () => {
    it('should return NotFound and error message if city is not found ', (done) => {
      const req = {
        params: {
          cityId: 287389, // non existent city id
        },
      };
      citiesAPI.routeToCityDetails(req, null, (error) => {
        assert.equal(error.message, constants.NOTFOUND_ERROR_MESSAGE);
        assert.equal(error.code, constants.NOTFOUND_ERROR);
        done();
      });
    });
    it('should call routeToCityDetails once', (done) => {
      const req = {
        params: {
          cityId: testConstants.TEST_CITYID, // non existent city id
        },
      };
      sinon.stub(cities, 'getCityDetails');
      citiesAPI.routeToCityDetails(req, null, () => {}); // do nothing in next
      sinon.assert.calledOnce(cities.getCityDetails); // check if it was exactly called once
      cities.getCityDetails.restore(); // restore original functionality
      done();
    });
  });
  describe('routeToWeather(req,res,next)', () => {
    it('should return NotFound and error message if city is not found ', (done) => {
      const req = {
        params: {
          cityId: 12345, // non existent city id
        },
      };
      citiesAPI.routeToWeather(req, null, (error) => {
        assert.equal(error.message, constants.NOTFOUND_ERROR_MESSAGE);
        assert.equal(error.code, constants.NOTFOUND_ERROR);
        done();
      });
    });
    it('should call routeToWeather once', (done) => {
      const req = {
        params: {
          cityId: testConstants.TEST_CITYID, // non existent city id
        },
      };
      sinon.stub(cities, 'getWeather');
      citiesAPI.routeToWeather(req, null, () => {}); // do nothing in next
      sinon.assert.calledOnce(cities.getWeather); // check if it was exactly called once
      cities.getWeather.restore(); // restore original functionality
      done();
    });
  });
});
