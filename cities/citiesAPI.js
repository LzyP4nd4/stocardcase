/* eslint-env node */
const errs = require('restify-errors');
const cities = require('./cities');
const constants = require('../lib/constants');

module.exports = {
  /**
   * handles the routes to /cities?lat&lng
   * @param {*} req request object
   * @param {*} res response object
   * @param {function} next next function for continuing
   * @returns json object containing all cities within radius or a
   * BadRequest error if params are missing
   */
  routeToCitiesInCycle(req, res, next) {
    const coords = {
      lat: parseFloat(req.query.lat),
      lon: parseFloat(req.query.lng),
    };
    if (Number.isNaN(coords.lat) || Number.isNaN(coords.lon)) {
      next(new errs.BadRequestError(constants.BADREQUEST_ERROR_MESSAGE));
    } else {
      cities.findCitiesInCycle(
        coords,
        (citiesInCycle) => {
          res.send(200, citiesInCycle);
        },
      );
      next();
    }
  },
  /**
   * handles the routes to /cities/:cityId
   * @param {*} req request object
   * @param {*} res response object
   * @param {function} next next function for continuing
   * @returns json object containing details about the city or a
   * NotFound error if city was not found
   */
  routeToCityDetails(req, res, next) {
    const cityId = parseInt(req.params.cityId, 10);
    if (Number.isNaN(cityId)) {
      next(new errs.NotFoundError(constants.NOTFOUND_ERROR_MESSAGE));
    } else {
      cities.getCityDetails(
        cityId,
        (cityDetails) => {
          if (cityDetails !== undefined) {
            res.send(cityDetails);
            next();
          } else {
            next(new errs.NotFoundError(constants.NOTFOUND_ERROR_MESSAGE));
          }
        },
      );
    }
  },
  /**
   * handles the routes to /cities/:cityId/weather
   * @param {*} req request object
   * @param {*} res response object
   * @param {function} next next function for continuing
   * @returns json object containing details about the weather of the city or a
   * NotFound error if city was not found
   */
  routeToWeather(req, res, next) {
    const cityId = parseInt(req.params.cityId, 10);
    if (Number.isNaN(cityId)) {
      next(new errs.NotFoundError(constants.NOTFOUND_ERROR_MESSAGE));
    } else {
      cities.getWeather(
        cityId,
        (weatherData) => {
          if (weatherData !== undefined) {
            res.send(weatherData);
            next();
          } else {
            next(new errs.NotFoundError(constants.NOTFOUND_ERROR_MESSAGE));
          }
        },
      );
    }
  },
};
