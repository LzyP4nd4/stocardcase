# StocardCase

Code Case for Interview

Structure of the Project:
|cities/
    |-test/
        |-*.js  (test files)
        |-*.json (postman collection file)
    |-*.js (JS scripts for the component (here cities feature))
|-config/
    |- .env (environment file for stuff like API Keys)
|-lib/
    |-city.list.json (list of the cities with coordinates)
    |-constants.js (constants, like strings used in production)
    |-constants_test.js (constants, like strings used in dev)
    |-utils.js (helper file for stuff like distance calculation & etc.)
|-app.js (Starting Point)
|-runTests.sh (Script to start the REST API and then start all tests and kill the rest API afterwards; 
|               only really useful for Docker at the moment)


Following libraries were used in addition to (restify, request):
- dotenv, for loading configuration files, like API keys
- restify-errors, for automatic error codes etc.

For development only:
- mocha, unit test framework
- sinon, stubbing and fakes in the unit tests
- eslint, for linting and fixing code style issues
- newman, for postman integration (API) tests in cli; the postman collection is exported as a json into the test folder

To run all tests in docker use following steps (in the root source folder) :
1) docker build -t ypeker/stocardcase .
2) docker run --rm ypeker/stocardcase

Otherwise you can use:
0) npm install
A) npm start 
-> to start the REST API service and use curl/browser/Postman 
    If services are running you can have integration tests with newman:
    A.2) ./node_modules/.bin/newman run cities/test/StocardCase.postman_collection.json
B) npm test
-> to execute the unit tests with mocha

